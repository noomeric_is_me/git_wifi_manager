package com.jao.freewifi.wifimanager.response;

public interface HostAsyncResponse extends LanHostAsyncResponse, WanHostAsyncResponse, ErrorAsyncResponse {
}
