package com.jao.freewifi.wifimanager.response;

public interface DnsAsyncResponse {
    void processFinish(String str);
}
