package com.jao.freewifi.wifimanager.response;

import com.jao.freewifi.wifimanager.network.Host;

public interface MainAsyncResponse extends ErrorAsyncResponse {
    void processFinish(int i);

    void processFinish(Host host);

    void processFinish(String str);

    void processFinish(boolean z);
}
