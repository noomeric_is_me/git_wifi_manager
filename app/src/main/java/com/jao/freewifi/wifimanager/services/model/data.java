package com.jao.freewifi.wifimanager.services.model;

/**
 * Created by pisit on 1/22/2017 AD.
 */

import java.util.List;

public class data {

    private String defaultads;
    private List<Ads> ads = null;
    private SpeedTestSharing speedTestSharing;

    public String getDefaultads() {
        return defaultads;
    }

    public void setDefaultads(String defaultads) {
        this.defaultads = defaultads;
    }

    public data withDefaultads(String defaultads) {
        this.defaultads = defaultads;
        return this;
    }

    public List<Ads> getAds() {
        return ads;
    }

    public void setAds(List<Ads> ads) {
        this.ads = ads;
    }

    public data withAds(List<Ads> ads) {
        this.ads = ads;
        return this;
    }

    public SpeedTestSharing getSpeedTestSharing() {
        return speedTestSharing;
    }

    public void setSpeedTestSharing(SpeedTestSharing speedTestSharing) {
        this.speedTestSharing = speedTestSharing;
    }
}