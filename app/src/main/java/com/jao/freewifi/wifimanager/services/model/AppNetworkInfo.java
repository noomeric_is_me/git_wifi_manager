package com.jao.freewifi.wifimanager.services.model;

/**
 * Created by pisit on 8/6/2017 AD.
 */

public class AppNetworkInfo {

    private NetworkInformation NetworkInformation;
    private External_IP External_IP;


    public NetworkInformation getNetworkInformation() {
        return NetworkInformation;
    }

    public void setNetworkInformation(NetworkInformation networkInformation) {
        NetworkInformation = networkInformation;
    }

    public External_IP getExternal_IP() {
        return External_IP;
    }

    public void setExternal_IP(External_IP external_IP) {
        External_IP = external_IP;
    }
}