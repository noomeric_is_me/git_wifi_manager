package com.jao.freewifi.wifimanager.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.widget.RelativeLayout;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSize;
import com.jao.freewifi.wifimanager.R;

public class Ad_Global {
    public static String AD_Banner = "2136258816673045_2136259143339679";
    public static String AD_Full = "2136258816673045_2136260736672853";
    public static String AD_Full_SPLASH = "2136258816673045_2136260930006167";

    @SuppressLint("WrongConstant")
    public static void loadBannerAd(Context context, final RelativeLayout relativeLayout) {
        if (isNetworkAvailable(context)) {
            relativeLayout.setVisibility(8);
            com.facebook.ads.AdView adView = new com.facebook.ads.AdView(context, context.getResources().getString(R.string.fb_id_ads_banner), AdSize.BANNER_HEIGHT_50);
            relativeLayout.addView(adView);
            adView.loadAd();
            adView.setAdListener(new com.facebook.ads.AdListener() {
                @Override
                public void onError(Ad ad, AdError adError) {
                    relativeLayout.setVisibility(8);
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    relativeLayout.setVisibility(0);
                }

                @Override
                public void onAdClicked(Ad ad) {
                    relativeLayout.setVisibility(0);
                }

                @Override
                public void onLoggingImpression(Ad ad) {
                }
            });
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            NetworkInfo[] allNetworkInfo = connectivityManager.getAllNetworkInfo();
            if (allNetworkInfo != null) {
                for (NetworkInfo state : allNetworkInfo) {
                    if (state.getState() == State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
