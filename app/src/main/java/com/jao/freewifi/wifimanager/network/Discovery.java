package com.jao.freewifi.wifimanager.network;

import com.jao.freewifi.wifimanager.async.ScanHostsAsyncTask;
import com.jao.freewifi.wifimanager.response.MainAsyncResponse;

public class Discovery {
    public static void scanHosts(int i, int i2, int i3, MainAsyncResponse mainAsyncResponse) {
        new ScanHostsAsyncTask(mainAsyncResponse).execute(i, i2, i3);
    }
}
