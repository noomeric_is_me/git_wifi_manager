package com.jao.freewifi.wifimanager.response;

interface ErrorAsyncResponse {
    <T extends Throwable> void processFinish(T t);
}
