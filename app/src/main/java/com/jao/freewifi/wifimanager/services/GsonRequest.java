package com.jao.freewifi.wifimanager.services;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pisit on 1/22/2017 AD.
 */

public class GsonRequest<T> extends Request<T> {

    private static final String ACCEPT = "Accept";
    private static final String ACCEPT_LANGUAGE = "Accept-Language";
    private static final String ACCEPT_ENCODING = "Accept-Encoding";
    private static final String USER_AGENT = "User-Agent";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String AUTHORIZATION = "Authorization";
    private static final String X_Auth_Token = "X-Auth-Token";

    private static final String APITOKEN = "65307277f88548188bb50ffced26c314";

    private final Gson gson = new Gson();
    private final Class<T> clazz;
    private final Map<String, String> headers;
    private final Response.Listener<T> listener;
    private JSONObject body;

    /**
     * Make a request and return a parsed object from JSON.
     *
     * @param url     URL of the request to make
     * @param clazz   Relevant class object, for Gson's reflection
     * @param headers Map of request headers
     */
    public GsonRequest(int method, String url, Class<T> clazz,
                       Map<String, String> headers, Response.Listener<T> listener,
                       Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.clazz = clazz;
        this.headers = headers;
        this.listener = listener;

    }



    public void setBodyJson(JSONObject json) {
        body = json;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        final Map<String, String> headers = new HashMap<>();
        headers.put(X_Auth_Token, APITOKEN);

        return headers != null ? headers : super.getHeaders();
    }


    @Override
    public byte[] getBody() throws AuthFailureError {
        // TODO Auto-generated method stub

        // String jsonString = gson.toJson(body, Gson.class);
        if (body != null) {
            // try {
            try {
                return body.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            // } catch (UnsupportedEncodingException e) {
            // // VolleyLog
            // //
            // .wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
            // // jsonString, PROTOCOL_CHARSET);
            //
            // e.printStackTrace();
            // return null;
        }
        // }else{
        return super.getBody();
        // }

    }

    @Override
    public String getBodyContentType() {
        // TODO Auto-generated method stub
        // return super.getBodyContentType();
        return "application/json";
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    public void deliverError(VolleyError error) {
        if (error instanceof NoConnectionError) {
            Cache.Entry entry = this.getCacheEntry();
            if(entry != null) {
                Response<T> response = parseNetworkResponse(new NetworkResponse(entry.data, entry.responseHeaders));
                deliverResponse(response.result);
                return;
            }
        }
        super.deliverError(error);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        // TODO Auto-generated method stub
        return super.getParams();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {

            String json = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));

            if(!json.substring(0,1).equalsIgnoreCase("{")){
                json = "{data:" + json + "}";
            }
            return Response.success(gson.fromJson(json, clazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}