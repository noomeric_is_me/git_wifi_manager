package com.jao.freewifi.wifimanager.services.model;

/**
 * Created by pisit on 8/5/2017 AD.
 */

public class NetworkInformation {

//    as: "AS17552 True Internet Co.,Ltd.",
//    city: "Bangkok",
//    country: "Thailand",
//    countryCode: "TH",
//    isp: "True Internet",
//    lat: 13.6667,
//    lon: 100.5833,
//    org: "True Internet",
//    query: "171.101.60.195",
//    region: "10",
//    regionName: "Bangkok",
//    status: "success",
//    timezone: "Asia/Bangkok",
//    zip: "10260"

    private String as;
    private String city;
    private String country;
    private String countryCode;
    private String isp;
    private String lat;
    private String lon;
    private String org;
    private String query;
    private String region;
    private String regionName;
    private String status;
    private String timezone;
    private String zip;

    public String getAs() {
        return as;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getIsp() {
        return isp;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getOrg() {
        return org;
    }

    public String getQuery() {
        return query;
    }

    public String getRegion() {
        return region;
    }

    public String getRegionName() {
        return regionName;
    }

    public String getStatus() {
        return status;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getZip() {
        return zip;
    }
}
