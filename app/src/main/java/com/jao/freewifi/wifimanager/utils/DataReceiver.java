package com.jao.freewifi.wifimanager.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DataReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (!com.jao.freewifi.wifimanager.utils.DataService.service_status) {
            context.startService(new Intent(context, com.jao.freewifi.wifimanager.utils.DataService.class));
        }
    }
}
